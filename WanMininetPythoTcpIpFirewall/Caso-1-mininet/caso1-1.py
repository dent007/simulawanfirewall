# -*- coding: utf-8 -*-
#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call

def myNetwork():

    net = Mininet( topo=None,
                   build=False,
                   ipBase='10.0.0.0/8')

    info( '*** Adding controller\n' )
    info( '*** Add switches lan y wan para cada borde de la univerdidad\n')
    s1_wing = net.addSwitch('s1_wing', cls=OVSKernelSwitch, failMode='standalone')
    s1_ling = net.addSwitch('s1_ling', cls=OVSKernelSwitch, failMode='standalone')
    s2_weco = net.addSwitch('s2_weco', cls=OVSKernelSwitch, failMode='standalone')
    s2_leco = net.addSwitch('s2_leco', cls=OVSKernelSwitch, failMode='standalone')
    s3_lsal = net.addSwitch('s3_lsal', cls=OVSKernelSwitch, failMode='standalone')
    s3_wsal = net.addSwitch('s3_wsal', cls=OVSKernelSwitch, failMode='standalone')
    s4_wfil = net.addSwitch('s4_wfil', cls=OVSKernelSwitch, failMode='standalone')
    s4_lfil = net.addSwitch('s4_lfil', cls=OVSKernelSwitch, failMode='standalone')

    info( '*** Add y prender / activar modo router en cada router \n')
    r_central = net.addHost('r_central', cls=Node, ip='')
    r_central.cmd('sysctl -w net.ipv4.ip_forward=1')
    r_ing = net.addHost('r_ing', cls=Node, ip='')
    r_ing.cmd('sysctl -w net.ipv4.ip_forward=1')
    r_eco = net.addHost('r_eco', cls=Node, ip='')
    r_eco.cmd('sysctl -w net.ipv4.ip_forward=1')
    r_sal = net.addHost('r_sal', cls=Node, ip='')
    r_sal.cmd('sysctl -w net.ipv4.ip_forward=1')
    r_fil = net.addHost('r_fil', cls=Node, ip='')
    r_fil.cmd('sysctl -w net.ipv4.ip_forward=1')

    info( '*** Add ip a hosts de cada facultad\n')
    h1_ing = net.addHost('h1_ing', cls=Host, ip='192.168.1.2/24', defaultRoute=None)
    h1_eco = net.addHost('h1_eco', cls=Host, ip='192.168.2.2/24', defaultRoute=None)
    h1_sal = net.addHost('h1_sal', cls=Host, ip='192.168.3.2/24', defaultRoute=None)
    h1_fil = net.addHost('h1_fil', cls=Host, ip='192.168.4.2/24', defaultRoute=None)

    info( '*** Add links entre routers y switchs (lan y wan) y asignarle interfaz e ip (gateway) a estas\n')
    net.addLink(r_central, s1_wing, intfName1='r_central-eth0', params1={ 'ip' : '192.168.100.6/29' })
    net.addLink(r_central, s2_weco, intfName1='r_central-eth1', params1={ 'ip' : '192.168.100.14/29' })
    net.addLink(r_central, s3_wsal, intfName1='r_central-eth2', params1={ 'ip' : '192.168.100.22/29' })
    net.addLink(r_central, s4_wfil, intfName1='r_central-eth3', params1={ 'ip' : '192.168.100.30/29' })

    net.addLink(r_ing, s1_wing, intfName1='r1-eth0', params1={ 'ip' : '192.168.100.1/29' })
    net.addLink(r_eco, s2_weco, intfName1='r1-eth0', params1={ 'ip' : '192.168.100.9/29' })
    net.addLink(r_sal, s3_wsal, intfName1='r1-eth0', params1={ 'ip' : '192.168.100.17/29' })
    net.addLink(r_fil, s4_wfil, intfName1='r1-eth0', params1={ 'ip' : '192.168.100.25/29' })
    
    net.addLink(r_ing, s1_ling, intfName1='r1-eth1', params1={ 'ip' : '192.168.1.1/24' })
    net.addLink(r_eco, s2_leco, intfName1='r1-eth1', params1={ 'ip' : '192.168.2.1/24' })
    net.addLink(r_sal, s3_lsal, intfName1='r1-eth1', params1={ 'ip' : '192.168.3.1/24' })
    net.addLink(r_fil, s4_lfil, intfName1='r1-eth1', params1={ 'ip' : '192.168.4.1/24' })
    
    info( '*** Add links entre switchs (lan) y host de cada borde\n')
    net.addLink(s1_ling, h1_ing)
    net.addLink(s2_leco, h1_eco )
    net.addLink(s3_lsal, h1_sal)
    net.addLink(s4_lfil, h1_fil)
    
    info( '*** Starting network\n')
    net.build()
    info( '*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info( '*** Starting switches\n')
    net.get('s1_wing').start([])
    net.get('s1_ling').start([])
    net.get('s2_weco').start([])
    net.get('s2_leco').start([])
    net.get('s3_wsal').start([])
    net.get('s3_lsal').start([])    
    net.get('s4_wfil').start([])
    net.get('s4_lfil').start([])    

    info( '*** Post configure switches and hosts\n')
    
    info( '*** Configuración saltos a otras redes. Borde Facultad de Ingenieria \n') 
    net['r_central'].cmd('ip route add 192.168.1.0/24 via 192.168.100.1')
    net['r_ing'].cmd('ip route add 0/0 via 192.168.100.6')
    net['h1_ing'].cmd('ip route add 0/0 via 192.168.1.1')

    info( '*** Configuración saltos a otras redes. Borde Facultad de Ciencias Economia \n') 
    net['r_central'].cmd('ip route add 192.168.2.0/24 via 192.168.100.9')
    net['r_eco'].cmd('ip route add 0/0 via 192.168.100.14')
    net['h1_eco'].cmd('ip route add 0/0 via 192.168.2.1')

    info( '*** Configuración saltos a otras redes. Borde Facultad de Ciencias de la Salud \n') 
    net['r_central'].cmd('ip route add 192.168.3.0/24 via 192.168.100.17')
    net['r_sal'].cmd('ip route add 0/0 via 192.168.100.22')
    net['h1_sal'].cmd('ip route add 0/0 via 192.168.3.1')


    info( '*** Configuración saltos a otras redes. Borde Facultad de Filosofía y Letras \n') 
    net['r_central'].cmd('ip route add 192.168.4.0/24 via 192.168.100.25')
    net['r_fil'].cmd('ip route add 0/0 via 192.168.100.30')
    net['h1_fil'].cmd('ip route add 0/0 via 192.168.4.1')


    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()

