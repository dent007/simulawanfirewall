# -*- coding: utf-8 -*-
# #!/usr/bin_/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call

def myNetwork():

    net = Mininet( topo=None,
                   build=False,
                   ipBase='10.0.0.0/8')

    info( '*** Adding controller\n' )
    info( '*** Add switches\n')
    s1_lan = net.addSwitch('s1_lan', cls=OVSKernelSwitch, failMode='standalone')
    s1_wan = net.addSwitch('s1_wan', cls=OVSKernelSwitch, failMode='standalone')
    s2_lan = net.addSwitch('s2_lan', cls=OVSKernelSwitch, failMode='standalone')
    s2_wan = net.addSwitch('s2_wan', cls=OVSKernelSwitch, failMode='standalone')

    r_central = net.addHost('r_central', cls=Node, ip='')
    r1 = net.addHost('r1', cls=Node, ip='')
    r2 = net.addHost('r2', cls=Node, ip='')

    r_central.cmd('sysctl -w net.ipv4.ip_forward=1')
    r1.cmd('sysctl -w net.ipv4.ip_forward=1')
    r2.cmd('sysctl -w net.ipv4.ip_forward=1')

    info( '*** Add hosts\n')
        
    h1 = net.addHost('h1', cls=Host, ip='10.0.1.254/24', defaultRoute=None)
    h2 = net.addHost('h2', cls=Host, ip='10.0.2.254/24', defaultRoute=None)

    info( '*** Add Reglas Firewall\n')    
   
    #info( '*** LIMPIAMOS REGLAR PARA EMPEZAR POR CADA PAQUETE EN FIREWALL r1\n')    

    net['r1'].cmd('iptables -F')
    net['r1'].cmd('iptables -t nat -F')
    net['r1'].cmd('iptables -X')
    net['r1'].cmd('iptables -Z')
   
    #info( '*** ESTABLECEMOS POLÍTICA POR DEFECTO, BLOQUEAR TODO\n')    
    
    net['r1'].cmd('iptables -P INPUT DROP')
    net['r1'].cmd('iptables -P OUTPUT DROP')
    net['r1'].cmd('iptables -P FORWARD DROP')

    net['r1'].cmd('iptables -A INPUT lo -j ACCEPT')
    net['r1'].cmd('iptables -A OUTPUT lo -j ACCEPT')

    #info( '***LEVANTANDO SERVIDOR Web DNS EN UNA MAQUÍNA h1 DE LA RED INTERNA]') 
    net['r1'].cmd('iptables -A FORWARD -p udp --dport 53 -j ACCEPT')
    net['h1'].cmd('echo serverDNS  | nc -u -l 53 &')
    net['h1'].cmd('echo serverHTTP | nc -l 80 &')

    #info( '*** RESTRINJIENDO ACCEDO DE PAQUETES DESDE WAN a ROUTER FIREWALL, FILTRANDO IP')
    net['r1'].cmd('iptables -A INPUT   -s 10.0.1.0/24  -i r1-eth1  -j ACCEPT')
    net['r1'].cmd('iptables -A OUTPUT  -d 10.0.1.0/24 -s 10.0.1.0/24 -m state --state ESTABLISHED -j ACCEPT')

    #info( '*** DEJO PASAR SOLO PAQUETES ip LOCALES A SERVICIOS: WEB (http:80 ) e EMAIL (pop3:110 | SMTP:25 ) )    
    net['r1'].cmd('iptables -A FORWARD -s 10.0.0.0/16 -d 10.0.0.0/16 -m state --state NEW,ESTABLISHED -j ACCEPT')
    net['r1'].cmd('iptables -A FORWARD  -m multiport --dports 80,110,25 -j ACCEPT')

    ### NAT de puertos
    net['r1'].cmd('iptables -t nat -A PREROUTING -i r1-eth0 -p tcp --dport 8080 -j DNAT --to 10.0.1.254:80')
    net['r1'].cmd('iptables -t nat -A PREROUTING -i r1-eth0 -p udp --dport 8081 -j DNAT --to 10.0.1.254:53')


    #info( '*** MISMAS REGLAS PARA ROUTER FIREWALL 2 ')    
    net['r2'].cmd('iptables -F')
    net['r2'].cmd('iptables -t nat -F')
    net['r2'].cmd('iptables -X')
    net['r2'].cmd('iptables -Z')
   
    #info( '*** ESTABLECEMOS POLÍTICA POR DEFECTO, BLOQUEAR TODO\n')    
    
    net['r2'].cmd('iptables -P INPUT DROP')
    net['r2'].cmd('iptables -P OUTPUT DROP')
    net['r2'].cmd('iptables -P FORWARD DROP')

    net['r2'].cmd('iptables -A INPUT lo -j ACCEPT')
    net['r2'].cmd('iptables -A OUTPUT lo -j ACCEPT')

    #info( '*** RESTRINJIENDO ACCEDO DE PAQUETES DESDE WAN a ROUTER FIREWALL')
    net['r2'].cmd('iptables -A INPUT   -s 10.0.2.0/24  -i r2-eth1  -j ACCEPT')
    net['r2'].cmd('iptables -A OUTPUT  -d 10.0.2.0/24 -s 10.0.2.0/24 -m state --state ESTABLISHED -j ACCEPT')

    #info( '*** DEJO PASAR SOLO PAQUETES ip LOCALES A SERVICIOS: WEB (http:80 ) e EMAIL (pop3:110 | SMTP:25 ) )    
    net['r2'].cmd('iptables -A FORWARD -s 10.0.0.0/16 -d 10.0.0.0/16 -m state --state NEW,ESTABLISHED -j ACCEPT')
    net['r2'].cmd('iptables -A FORWARD  -m multiport --dports 80,110,25 -j ACCEPT')
    

    info( '*** Add links\n')
    # addLink( <[Host|str]>node1, <[Host|str]>node2,<int>port1, <int>port2, <Link>cls, <str>**params) */
    net.addLink(r_central, s1_wan, intfName1='r_central-eth0', params1={ 'ip' : '192.168.100.6/29' })
    net.addLink(r_central, s2_wan, intfName1='r_central-eth1', params1={ 'ip' : '192.168.100.14/29' })

    net.addLink(r1, s1_wan, intfName1='r1-eth0', params1={ 'ip' : '192.168.100.1/29' })
    net.addLink(r2, s2_wan, intfName1='r2-eth0', params1={ 'ip' : '192.168.100.9/29' })

    net.addLink(r1, s1_lan, intfName1='r1-eth1', params1={ 'ip' : '10.0.1.1/24' })
    net.addLink(r2, s2_lan, intfName1='r2-eth1', params1={ 'ip' : '10.0.2.1/24' })
    
    net.addLink(h1, s1_lan)
    net.addLink(h2, s2_lan)

    info( '*** Starting network\n')
    net.build()
    info( '*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info( '*** Starting switches\n')
    net.get('s1_lan').start([])
    net.get('s1_wan').start([])
    net.get('s2_wan').start([])
    net.get('s2_lan').start([])

    info( '*** Post configure switches and hosts\n')
    net['r_central'].cmd('ip route add 10.0.1.0/24 via 192.168.100.1')
    net['r_central'].cmd('ip route add 10.0.2.0/24 via 192.168.100.9')

    net['r1'].cmd('ip route add 0/0 via 192.168.100.6')
    net['r2'].cmd('ip route add 0/0 via 192.168.100.14')
    
    net['h1'].cmd('ip route add 0/0 via 10.0.1.1')
    net['h2'].cmd('ip route add 0/0 via 10.0.2.1')

    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()


"""
**** Accediendo a serivios en h1 desde h2 SIN port forwarding 

mininet> h2 telnet h1 80
Trying 10.0.1.254...
Connected to 10.0.1.254.
Escape character is '^]'.
serverHTTP
 

mininet> h2 nc -vu 10.0.1.254 53
Connection to 10.0.1.254 53 port [udp/domain] succeeded!
serverDNS


**** Accediendo a serivios en h1 desde h2 CON  port forwarding via r1 
mininet> h2 telnet h1 8080
Trying 10.0.1.254...
Connected to 10.0.1.254.
Escape character is '^]'.
serverHTTP


mininet> h2 nc -vu 10.0.1.254 8081
Connection to 10.0.1.254 8081 port [udp/*] succeeded!
serverDNS

 netcat:
 -l: Indica a netcat que debe permanecer a la escucha.
-p: Para indicar el puerto de origen.
-s: Para indicar la dirección origen.
-u: Netcat abre el puerto como UDP en vez de TCP (que es por defecto).
-v: Con esta opción nos mostrará información de la conexión.
 """